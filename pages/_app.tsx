import '../styles/globals.css'
import type { AppProps } from 'next/app'

export default function App({ Component, pageProps }: AppProps) {
  return (
    <div className="h-auto p-2 min-h-screen bg-indigo-100 flex justify-center items-center">
      <div className="lg:w-1/2 md:w-2/3 sm:w-full flex flex-col gap-4 justify-center items-center">
          <Component {...pageProps} />
      </div>
    </div>
  )
}
